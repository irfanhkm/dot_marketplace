@extends('template')
@section('tittle')
    Category
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Form @yield('tittle')
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tabel Product</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{session('success')}}</div>
                        @endif
                        <table class="table table-bordered">
                            <thead>
                            <th>Id Produk</th>
                            <th>Nama Produk</th>
                            <th>Aksi</th>
                            </thead>
                            <tbody>
                            @forelse($category->products as $product)
                                <tr>
                                    <td>{{ $product->id }}</td>
                                    <td>{{ $product->nama }}</td>
                                    @if($product->trashed())
                                        <td>
                                            <a href="/category/{{$product->id}}/restoreProduct" class="btn btn-primary">
                                                <span class="fa fa-history"></span>
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2" align="center">
                                        Belum ada produk dalam kategori ini.
                                    </td>
                                </tr>
                            @endforelse
                            <tr>
                                <td>
                                    <a href="{{url('category')}}" class="btn btn-primary">Back</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>
@endsection