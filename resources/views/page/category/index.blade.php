@extends('template')
@section('tittle')
    Category
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Form @yield('tittle')
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @include('base/alert')
                <div class="box box-primary">
                    <!-- form start -->
                    <form role="form" method="post" action="/category" id="form">
                        @csrf
                        <div class="box-body">
                            <div class="form-group @if($errors->first('name')) has-error @endif">
                                <label>Nama Kategori</label>
                                <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Masukkan nama kategori" id="name">
                                <span class="help-block">{{ $errors->first('name') }}</span>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tabel @yield('tittle')</h3>
                        <span class="pull-right">
                            <form action="/category" method="get">
                                <input class="form-control" type="text" placeholder="Search here" name="search" value="{{old('search')}}">
                            </form>
                        </span>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Nama Kategori</th>
                                <th>Jumlah Product</th>
                                <th>Aksi</th>
                            </tr>
                            @forelse($dataCategory as $key => $value)
                                <tr>
                                    <td>{{++$number}}</td>
                                    <td>{{$value->name}}</td>
                                    @if($value->product_count)
                                    <td>{{$value->product_count}}</td>
                                        @else
                                        <td>0</td>
                                    @endif
                                    <td>
                                        <a href="/category/{{$value->id}}" class="btn btn-warning" style="float: left; margin-right: 1%"><span class="fa fa-eye"></span></a>
                                        <a href="/category/{{$value->id}}/edit" class="btn btn-primary" style="float:left; margin-right: 1%"><span class="fa fa-edit"></span></a>
                                        @if(!$value->trashed())
                                            <form action="{{url('category/'.$value->id)}}" method="post" style="">
                                                {{ method_field('DELETE') }}
                                                @csrf
                                                <input type="hidden" name="id" value="{{$value->id}}">
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Data yang dihapus tidak dapat dikembalikan, Anda yakin ?')"><span class="fa fa-trash"></span></button>
                                            </form>
                                        @endif
                                        @if ($value->trashed())
                                            <a href="/category/{{$value->id}}/restoreKategori" class="btn btn-primary"><span class="fa fa-history"></span></a>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" align="center">Data tidak ada</td>
                                </tr>
                            @endforelse
                        </table>
                    </div>

                    <div class="text-center">
                        {!! $dataCategory->appends(request()->all())->links() !!}
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>
@endsection
@section('js')
    <script>
        $('#form input').on('keyup',function() {
            $(this).parents('.form-group').removeClass('has-error').addClass('has-success').find('.help-block').html('');
        })
    </script>
@endsection