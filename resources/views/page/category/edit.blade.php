@extends('template')
@section('tittle')
    Category
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Form @yield('tittle')
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tabel @yield('tittle')</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        @if(count($errors->all())>0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                {{$message}} <br>
                                @endforeach
                            </div>
                        @endif
                        <table class="table table-bordered">
                            <form action="/category/{{$dataCategory->id}}" method="post">
                                @csrf
                                @method('PUT')
                                <tr>
                                    <td>Nama</td>
                                    <td>
                                        <input class="form-control" name="name" value="{{$dataCategory->name}}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <button class="btn btn-success" type="submit" >Simpan</button>
                                        <a href="{{url('category')}}" class="btn btn-primary">Back</a>
                                    </td>
                                </tr>
                            </form>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>
@endsection