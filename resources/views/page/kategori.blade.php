@extends('template')
@section('tittle')
    Kategori
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Form Kategori
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="{{url('kategori')}}">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Kategori</label>
                                <input type="text" class="form-control" name="kategori" placeholder="Masukkan nama kategori produk">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tabel Kategori</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Nama Kategori</th>
                            </tr>
                            @if (Session('kategori'))
                                <?php
                                    if (Session('dataSorted'))
                                    {
                                        $dataKategori = session('dataSorted');
                                    }else{
                                        $dataKategori = session('kategori');
                                    }
                                ?>
                                @foreach(session('kategori') as $key=>$value)
                                        <tr>
                                            <td>{{$key}}</td>
                                            <td>{{$value}}</td>
                                            <td>
                                                <button class="btn btn-primary" onclick="edit('{{$value}}','{{$key}}')" data-toggle="modal" data-target="#modal-edit"><span class="fa fa-edit"></span></button>
                                                <a href="{{url('kategori/delete/'.$key)}}" class="btn btn-danger"><span class="fa fa-trash"></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>
@endsection
<div class="modal fade" id="modal-edit" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Modal Edit</h4>
            </div>
            <div class="modal-body">
                <form action="{{url('kategori/edit')}}" method="post">
                    <input type="hidden" name="key" id="key">
                    <div class="form-group">
                        <input type="text" class="form-control" name="kategori" id="kategori">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-submit">Edit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@section('js')
    <script>
        function edit(value,key) {
            $('#kategori').val(value);
            $('#key').val(key);
        }
    </script>
@endsection