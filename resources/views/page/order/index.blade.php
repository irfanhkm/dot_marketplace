@extends('template')
@section('tittle')
    Order
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tabel @yield('tittle')
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">{{session('success')}}</div>
                @endif
                <div class="box">
                    <div class="box-header with-border">
                        <form action="/order" method="post">
                            @csrf
                            <div class="form-group @if($errors->first('customer_id')) has-error @endif">
                                <label></label>
                                <select class="form-control" name="customer_id">
                                    <option value="">Pilih Customer</option>
                                    @foreach($dataCustomer as $item)
                                        @if(old('customer_id') == $item->id )
                                            <option value="{{ $item->id }}"
                                                    selected>{{ $item->first_name .' '.$item->last_name }}</option>
                                        @else
                                            <option value="{{ $item->id }}">{{ $item->first_name .' '.$item->last_name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('customer_id') }}</span>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box">
                    <div class="box">
                        <div class="box-header with-border">
                            {{--                            <span class="pull-left">--}}
                            {{--                                 <a href="/order/create" class="btn btn-primary">Tambah Data</a>--}}
                            {{--                            </span>--}}
                            <span class="pull-right">
                                <form action="/order" method="get">
                                    <input class="form-control" type="text" placeholder="Search here" name="search" value="{{old('search')}}">
                                </form>
                            </span>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Customer</th>
                                    <th>Total Bayar</th>
                                    <th>Aksi</th>
                                </tr>
                                @forelse($dataOrder as $key => $value)
                                    <tr>
                                        <td>{{++$number}}</td>
                                        <td>{{$value->customers->first_name}} {{$value->customers->last_name}}</td>
                                        <td>Rp {{number_format($value->total)}}</td>
                                        <td>
                                            <a href="{{'/order/'.$value->id}}" class="btn btn-warning"><span
                                                        class="fa fa-eye"></span></a>
                                            @if($value->trashed())
                                                <a href="{{'/order/restore/'.$value->id}}" class="btn btn-primary"><span
                                                            class="fa fa-history"></span></a>
                                            @else
                                                <a href="{{'/order/delete/'.$value->id}}" class="btn btn-danger"
                                                   onclick="return confirm('Data yang dihapus tidak dapat dikembalikan, Anda yakin ?')"><span
                                                            class="fa fa-trash"></span></a>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5" align="center">Data tidak ada</td>
                                    </tr>
                                @endforelse
                            </table>
                        </div>

                        <div class="text-center">
                            {!! $dataOrder->appends(request()->all())->links() !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection