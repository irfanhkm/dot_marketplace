@extends('template')
@section('tittle')
    Detail Order
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Form @yield('tittle')
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-borer">
                        <h3 class="box-title">Form Product</h3>
                    </div>
                    <div class="box-body">
                        <form action="/order/addProduct/{{$dataOrder->id}}" method="post">
                            @csrf
                            <div class="form-group">
                                <input type="disabled" readonly class="form-control"
                                       value="{{$dataOrder->customers->first_name}} {{$dataOrder->customers->last_name}}">
                            </div>
                            <div class="form-group @if($errors->first('product_id')) has-error @endif">
                                <select class="form-control" name="product_id">
                                    <option value="">Pilih Product</option>
                                    @foreach($dataProduct as $item)
                                        @if(old('product_id') == $item->id )
                                            <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                                        @else
                                            <option value="{{ $item->id }}">{{ $item->nama }} - Rp {{ number_format($item->unit_price,2,",",".") }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('product_id') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('qty')) has-error @endif">
                                <input value="{{old('qty')}}" type="number" class="form-control" name="qty"
                                       placeholder="Masukkan qty barang">
                                <span class="help-block">{{ $errors->first('qty') }}</span>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tabel @yield('tittle')</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{session('success')}}</div>
                        @endif
                        <table class="table table-bordered">
                            <thead>
                            <th>Nama Produk</th>
                            <th>Harga Satuan</th>
                            <th>Quantity</th>
                            <th>Total</th>
                            </thead>
                            <tbody>
                            @if(count($dataOrderDetails))
                                @foreach($dataOrderDetails as $order)
                                    <tr>
                                        <td>{{ $order->products->nama }}</td>
                                        <td>Rp {{ number_format($order->products->unit_price,2,',','.') }}</td>
                                        <td>{{ $order->quantity }}</td>
                                        <td>
                                            Rp {{ number_format($order->quantity * $order->products->unit_price,2,',','.') }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="2">
                                        <a href="/order" class="btn btn-primary">Back</a>
                                    </td>
                                    <td colspan="1">
                                        Grand Total
                                    </td>
                                    <td>Rp {{number_format($total,2,",",".")}}</td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="4" align="center">Data tidak ditemukan</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>
@endsection