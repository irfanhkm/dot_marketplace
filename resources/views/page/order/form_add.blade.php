@extends('template')
@section('tittle')
    Customer
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Form @yield('tittle')
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        @if(count($errors->all())>0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    {{$message}} <br>
                                @endforeach
                            </div>
                        @endif
                        <form action="/order" method="post" id="form">
                            @csrf
                            <div class="form-group" id="pilih-jenis">
                                <button type="button" onclick="customer()" class="btn btn-primary">Customer</button>
                                <button type="button" onclick="customerGuest()" class="btn btn-primary">Guest</button>
                            </div>
                            <div class="form-group" id="customer" style="display: none;">
                                <label>Customer</label>
                                <select class="form-control" id="input-customer">
                                    <option value="">Pilih Customer</option>
                                    @foreach($dataCustomer as $item)
                                        @if(old('customer_id') == $item->id )
                                            <option value="{{ $item->id }}" selected>{{ $item->first_name .' '.$item->last_name }}</option>
                                        @else
                                            <option value="{{ $item->id }}">{{ $item->first_name .' '.$item->last_name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" id="guest" style="display: none;">
                                <label>Guest</label>
                                <input type="text" class="form-control" id="input-guest"
                                       placeholder="Masukan Nama Guest">
                                <input type="hidden" name="guest" id="input-hidden-guest">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="submit">Submit</button>
                                <a href="/order" class="btn btn-warning">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $('#form input').on('keyup', function () {
                $(this).parents('.form-group').removeClass('has-error').addClass('has-success').find('.help-block').html('');
            });
            $('#form select').on('change', function () {
                $(this).parents('.form-group').removeClass('has-error').addClass('has-success').find('.help-block').html('');
            });
        });

        function customerGuest() {
            $('#guest').show();
            $('#input-hidden-guest').val(1);
            $('#input-guest').attr('name', 'customer_id');
            $('#pilih-jenis').hide();
        }

        function customer() {
            $('#customer').show();
            $('#input-customer').attr('name', 'customer_id');
            $('#pilih-jenis').hide();
        }
    </script>
@endsection