@extends('template')
@section('tittle')
    Product
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Form Product
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- form start -->
                    <form role="form" method="post" action="{{url('product')}}">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Nama Produk</label>
                                <input type="text" class="form-control" name="nama" placeholder="Masukkan nama produk">
                            </div>
                            <div class="form-group">
                                <label>Kategori Produk</label>
                                <select type="text" class="form-control" name="kategori">
                                    <option style="display: none;">Pilih Kategori</option>
                                    @if(session('kategori'))
                                        <?php
                                            //sorting select data
                                            $data = session('kategori');
                                            asort($data);
                                        ?>
                                        @foreach($data as $key => $value)
                                            <option value="{{$value}}">{{$value}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Harga</label>
                                <input type="number" class="form-control" name="harga" placeholder="Masukkan harga produk">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tabel Product</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Nama Product</th>
                                <th>Kategori</th>
                                <th>Harga</th>
                            </tr>
                            @if (session('product'))
                                <?php
                                if (session('dataProductSorted'))
                                {
                                    $dataProduct = session('dataProductSorted');
                                }else{
                                    $dataProduct = session('product');
                                }
                                ?>
                                @foreach($dataProduct as $key => $value)
                                    <tr>
                                        <td>{{$key}}</td>
                                        <td>{{$value['nama']}}</td>
                                        <td>{{$value['kategori']}}</td>
                                        <td>Rp {{$value['harga']}}</td>
                                        <td>
                                            <button class="btn btn-primary" onclick="edit('{{$key}}','{{$value['nama']}}','{{$value['kategori']}}','{{$value['harga']}}')" data-toggle="modal" data-target="#modal-edit"><span class="fa fa-edit"></span></button>
                                            <a href="{{url('product/delete/'.$key)}}" class="btn btn-danger"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>
@endsection
<div class="modal fade" id="modal-edit" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Modal Edit</h4>
            </div>
            <div class="modal-body">
                <form action="{{url('product/edit')}}" method="post">
                    <input type="hidden" name="key" id="key">
                    <div class="form-group">
                        <label>Nama Produk</label>
                        <input id="nama" type="text" class="form-control" name="nama" placeholder="Masukkan nama produk">
                    </div>
                    <div class="form-group">
                        <label>Kategori Produk</label>
                        <select id="kategori" type="text" class="form-control" name="kategori">
                            <option style="display: none;">Pilih Kategori</option>
                            @if(session('kategori'))
                                <?php
                                //sorting select data
                                $data = session('kategori');
                                asort($data);
                                ?>
                                @foreach($data as $key => $value)
                                    <option value="{{$value}}">{{$value}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Harga</label>
                        <input id="harga" type="number" class="form-control" name="harga" placeholder="Masukkan harga produk">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-submit">Edit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@section('js')
    <script>
        function edit(key,nama,kategori,harga) {
            $('#nama').val(nama);
            $('#kategori').val(kategori);
            $('#harga').val(harga);
            $('#key').val(key);
        }
    </script>
@endsection