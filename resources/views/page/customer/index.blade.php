@extends('template')
@section('tittle')
    Customer
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tabel @yield('tittle')
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @include('base/alert')
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="header-tittle">Form @yield('tittle')</h4>
                    </div>
                    <div class="box-body">
                        <form action="/customer" method="post" id="form">
                            @csrf
                            <div class="form-group @if($errors->first('email')) has-error @endif">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" value="{{old('email')}}"
                                       placeholder="Masukkan email">
                                <span class="help-block">{{ $errors->first('email') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('first_name')) has-error @endif">
                                <label>Nama Depan</label>
                                <input type="text" class="form-control" name="first_name" value="{{old('first_name')}}"
                                       placeholder="Masukkan nama depan">
                                <span class="help-block">{{ $errors->first('first_name') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('last_name')) has-error @endif">
                                <label>Nama Belakang</label>
                                <input type="text" class="form-control" name="last_name" value="{{old('last_name')}}"
                                       placeholder="Masukkan nama belakang">
                                <span class="help-block">{{ $errors->first('last_name') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('address')) has-error @endif">
                                <label>Alamat</label>
                                <textarea class="form-control" name="address"
                                          placeholder="Masukkan alamat">{{old('address')}}</textarea>
                                <span class="help-block">{{ $errors->first('address') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('phone_number')) has-error @endif">
                                <label>No Hp</label>
                                <input type="number" class="form-control" name="phone_number"
                                       value="{{old('phone_number')}}" placeholder="Masukkan nomor hp">
                                <span class="help-block">{{ $errors->first('phone_number') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('password')) has-error @endif">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password"
                                       placeholder="Masukkan password">
                                <span class="help-block">{{ $errors->first('password') }}</span>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header with-border">
                         <span class="pull-left">
                             <h4 class="header-tittle">Tabel @yield('tittle')</h4>
                         </span>
                         <span class="pull-right">
                                <form action="/customer/search" method="get">
                                    <input class="form-control" type="text" placeholder="Search here" name="search" value="{{old('search')}}">
                                </form>
                         </span>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Nama Customer</th>
                                <th>Email</th>
                                <th>Alamat</th>
                                <th>Aksi</th>
                            </tr>
                            @forelse($dataCustomer as $key => $value)
                                <tr>
                                    <td>{{++$number}}</td>
                                    <td>{{$value->first_name.' '.$value->last_name}}</td>
                                    <td>{{$value->email}}</td>
                                    <td>{{$value->address}}</td>
                                    <td>
                                        <a href="{{url('customer/'.$value->id)}}" class="btn btn-warning" style="float: left;margin-right: 1%"><span class="fa fa-eye"></span></a>
                                        <a href="{{url('customer/'.$value->id.'/edit')}}" class="btn btn-primary" style="float: left;margin-right: 1%"><span class="fa fa-edit"></span></a>
                                        <form action="{{url('customer/'.$value->id)}}" method="post">
                                            {{ method_field('DELETE') }}
                                            @csrf
                                            <input type="hidden" name="id" value="{{$value->id}}">
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Daat yang dihapus tidak dapat dikembalikan, Anda yakin ?')"><span class="fa fa-trash"></span></button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" align="center">Data tidak ada</td>
                                </tr>
                            @endforelse
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>
@endsection
@section('js')
    <script>
        $('#form input').on('keyup',function() {
            $(this).parents('.form-group').removeClass('has-error').addClass('has-success').find('.help-block').html('');
        });
        $('#form textarea').on('keyup', function () {
            $(this).parents('.form-group').removeClass('has-error').addClass('has-success').find('.help-block').html('');
        })
    </script>
@endsection