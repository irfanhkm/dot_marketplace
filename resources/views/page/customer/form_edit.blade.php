@extends('template')
@section('tittle')
    Product
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Form Product
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @include('base/alert')
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tabel @yield('tittle')</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{url('customer/'.$dataCustomer->id)}}" method="post" id="form">
                            @csrf
                            @method('put')
                            <div class="form-group @if($errors->first('email')) has-error @endif">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" value="{{$dataCustomer->email}}"
                                       placeholder="Masukkan email">
                                <span class="help-block">{{ $errors->first('email') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('first_name')) has-error @endif">
                                <label>Nama Depan</label>
                                <input type="text" class="form-control" name="first_name" value="{{$dataCustomer->first_name}}" placeholder="Masukkan nama depan">
                                <span class="help-block">{{ $errors->first('first_name') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('last_name')) has-error @endif">
                                <label>Nama Belakang</label>
                                <input type="text" class="form-control" name="last_name" value="{{$dataCustomer->last_name}}" placeholder="Masukkan nama belakang">
                                <span class="help-block">{{ $errors->first('last_name') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('address')) has-error @endif">
                                <label>Alamat</label>
                                <textarea class="form-control" name="address"
                                          placeholder="Masukkan alamat">{{$dataCustomer->address}}</textarea>
                                <span class="help-block">{{ $errors->first('address') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('phone_number')) has-error @endif">
                                <label>No Hp</label>
                                <input type="number" class="form-control" name="phone_number" value="{{$dataCustomer->phone_number}}" placeholder="Masukkan nomor hp">
                                <span class="help-block">{{ $errors->first('phone_number') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('password')) has-error @endif">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Masukkan password">
                                <span class="help-block">{{ $errors->first('password') }}</span>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{$dataCustomer->id}}">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a href="{{url('customer')}}" class="btn btn-primary">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>
@endsection
@section('js')
    <script>
        $('#form input').on('keyup',function() {
            $(this).parents('.form-group').removeClass('has-error').addClass('has-success').find('.help-block').html('');
        });
        $('#form textarea').on('keyup',function() {
            $(this).parents('.form-group').removeClass('has-error').addClass('has-success').find('.help-block').html('');
        })
    </script>
@endsection