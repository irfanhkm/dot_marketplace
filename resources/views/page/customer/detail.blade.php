@extends('template')
@section('tittle')
    Customer {{$dataCustomer->first_name .' '. $dataCustomer->last_name}}
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Detail @yield('tittle')
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tabel @yield('tittle')</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label>Email</label>
                            <input readonly class="form-control" value="{{$dataCustomer->email}}">
                        </div>
                        <div class="form-group">
                            <label>Nama Depan</label>
                            <input readonly class="form-control" value="{{$dataCustomer->first_name}}">
                        </div>
                        <div class="form-group">
                            <label>Nama Belakang</label>
                            <input readonly class="form-control" name="last_name" value="{{$dataCustomer->last_name}}">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea readonly class="form-control">{{$dataCustomer->address}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>No Hp</label>
                            <input readonly class="form-control" value="{{$dataCustomer->phone_number}}">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input readonly class="form-control" value="{{$dataCustomer->password}}">
                        </div>
                        <div class="form-group">
                            <a href="{{url('customer')}}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection