@extends('template')
@section('tittle')
    Product
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Form Product
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @include('base/alert')
                <div class="box box-primary">
                    <!-- form start -->
                    <form role="form" method="post" action="{{url('product/store')}}" id="form" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="form-group @if($errors->first('nama')) has-error @endif">
                                <label>Nama Produk</label>
                                <input type="text" class="form-control" name="nama" value="{{old('nama')}}" placeholder="Masukkan nama produk" id="nama">
                                <span class="help-block">{{ $errors->first('nama') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('kategori_id')) has-error @endif">
                                <label>Kategori Produk</label>
                                <select class="form-control" name="kategori_id" value="{{old('kategori_id')}}">
                                    <option value="">Pilih Kategori</option>
                                    @foreach($dataCategory as $item)
                                        @if(old('kategori_id') == $item->id )
                                            <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                                        @else
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('kategori_id') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('unit_price')) has-error @endif">
                                <label>Harga</label>
                                <input type="number" class="form-control" name="unit_price" value="{{old('unit_price')}}" placeholder="Masukkan harga produk" id="unit_price">
                                <span class="help-block">{{ $errors->first('unit_price') }}</span>
                            </div>
                            <div class="form-group @if($errors->first('photo')) has-error @endif">
                                <label>Foto Produk</label>
                                <input type="file" name="photo" class="form-control">
                                <span class="help-block">{{ $errors->first('photo') }}</span>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tabel Product</h3>
                        <span class="pull-right">
                            <form action="" method="get">
                                <input class="form-control" type="text" placeholder="Search here" name="search" value="{{old('search')}}">
                            </form>
                        </span>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Nama Product</th>
                                <th>Kategori</th>
                                <th>Harga</th>
                                <th>Foto</th>
                                <th>Aksi</th>
                            </tr>
                            @forelse($dataProduct as $key => $value)
                                <tr>
                                    <td>{{++$number}}</td>
                                    <td>{{$value->nama}}</td>
                                    <td>{{$value->category->name}}</td>
                                    <td>Rp {{number_format($value->unit_price,'2',',','.')}}</td>
                                    <td><img src="{{asset('images/'.$value->photo)}}" width="100px"></td>
                                    <td>
                                        <a href="{{url('product/'.$value->id)}}" class="btn btn-warning"><span class="fa fa-eye"></span></a>
                                        <a href="{{url('product/'.$value->id.'/edit')}}" class="btn btn-primary"><span class="fa fa-edit"></span></a>
                                        <a href="{{url('product/delete/'.$value->id)}}" class="btn btn-danger" onclick="return confirm('Daat yang dihapus tidak dapat dikembalikan, Anda yakin ?')"><span class="fa fa-trash"></span></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" align="center">Data tidak ada</td>
                                </tr>
                            @endforelse
                        </table>
                    </div>

                    <div class="text-center">
                        {!! $dataProduct->appends(request()->all())->links() !!}
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>
@endsection
@section('js')
    <script>
        $('#form input').on('keyup',function() {
            $(this).parents('.form-group').removeClass('has-error').addClass('has-success').find('.help-block').html('');
        });
        $('#form select').on('change',function() {
            $(this).parents('.form-group').removeClass('has-error').addClass('has-success').find('.help-block').html('');
        })
    </script>
@endsection