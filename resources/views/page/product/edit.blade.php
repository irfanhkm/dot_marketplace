@extends('template')
@section('tittle')
    Product
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Form Product
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tabel Product</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        @if(count($errors->all())>0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                {{$message}} <br>
                                @endforeach
                            </div>
                        @endif
                        <table class="table table-bordered">
                            <form action="{{url('product/'.$dataProduct->id.'/update')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <tr>
                                    <td>Nama</td>
                                    <td>
                                        <input class="form-control" name="nama" value="{{$dataProduct->nama}}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kategori</td>
                                    <td>
                                        <select class="form-control" name="kategori_id" id="kategori_id">
                                            <option value="">Pilih Kategori</option>
                                            @foreach($dataCategory as $item)
                                                <option value="{{$item->id}}" {{($dataProduct->kategori_id == $item->id) ? 'selected' : '' }}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Harga</td>
                                    <td>
                                        <input class="form-control" name="unit_price" value="{{$dataProduct->unit_price}}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Foto</td>
                                    <td>
                                        <img src="/images/{{$dataProduct->photo}}" width="30%" id="photo">
                                        <input type="hidden" name="photos" value="{{$dataProduct->photo}}" id="input-photo">
                                        <span onclick="change()" class="btn btn-success" type="submit" id="tombol-ganti">Ganti Foto</span>
                                        <span onclick="batal()" class="btn btn-danger" type="submit" id="tombol-batal" style="display: none">Batal Ubah Foto</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="hidden" name="id" value="{{$dataProduct->id}}">
                                        <button class="btn btn-success" type="submit">Simpan</button>
                                    </td>
                                    <td><a href="{{url('product')}}" class="btn btn-primary">Back</a></td>
                                </tr>
                            </form>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>
@endsection
@section('js')
    <script>
        function change() {
            $("#photo").hide();
            $("#tombol-ganti").hide();
            $('#input-photo').attr('type','file');
            $('#input-photo').attr('name','photo');
            $('#input-photo').show();
            $('#tombol-batal').show();
        }
        function batal(){
            $('#tombol-batal').hide();
            $("#tombol-ganti").show();
            $('#input-photo').attr('type','hidden');
            $('#input-photo').attr('name','photos');
            $('#input-photo').attr('value','{{$dataProduct->photo}}');
            $('#photo').show();
        }
    </script>
    @endsection
