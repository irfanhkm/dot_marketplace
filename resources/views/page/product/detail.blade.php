@extends('template')
@section('tittle')
    Product
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Form Product
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tabel Product</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <td>Nama</td>
                                <td>{{$dataProduct->nama}}</td>
                            </tr>
                            <tr>
                                <td>Kategori</td>
                                <td>{{$dataProduct->category->name}}</td>
                            </tr>
                            <tr>
                                <td>Harga</td>
                                <td>Rp {{$dataProduct->unit_price}}</td>
                            </tr>
                            <tr>
                                <td>Gambar</td>
                                <td>
                                    @if($dataProduct->photo)
                                        <img src="/images/{{$dataProduct->photo}}" width="50%">
                                    @else
                                        Gambar tidak ada
                                    @endif
                                </td>
                            </tr>
                            <td>
                                <a href="{{url('product')}}" class="btn btn-primary">Back</a>
                            </td>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </section>
@endsection