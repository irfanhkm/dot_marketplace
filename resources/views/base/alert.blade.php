@if(Session::has('success'))
    <div class="alert alert-success">{{session('success')}}</div>
@endif
@if(Session::has('fail'))
    <div class="alert alert-danger">{{session('fail')}}</div>
@endif