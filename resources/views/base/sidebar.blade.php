<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU</li>
            <li class="{{Request::route()->getPrefix('product') ? 'active' : ''}}">
                <a href="{{url('product')}}">
                    <i class="fa fa-circle-o"></i> <span>Product</span>
                </a>
            </li>
            <li class="{{in_array(Request::route()->getName(),["customer.edit","customer.index","customer.create"]) ? 'active' : ''}}">
                <a href="{{url('customer')}}">
                    <i class="fa fa-user-o"></i> <span>Customer</span>
                </a>
            </li>
            <li class="{{in_array(Request::route()->getName(),["order.edit","order.index","order.create","order.show"]) ? 'active' : ''}}">
                <a href="{{url('order')}}">
                    <i class="fa fa-sticky-note-o"></i> <span>Order</span>
                </a>
            </li>
            <li class="{{in_array(Request::route()->getName(),["category.edit","category.index","category.create","category.show"]) ? 'active' : ''}}">
                <a href="{{url('category')}}">
                    <i class="fa fa-book"></i> <span>Category</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>