#DOT Marketplace

Server Requirement (Laravel 5.8)
PHP >= 7.1.3
OpenSSL PHP Extension
PDO PHP Extension
Mbstring PHP Extension
Tokenizer PHP Extension
XML PHP Extension
Ctype PHP Extension
JSON PHP Extension
BCMath PHP Extension

How to use this

1. Download and install xampp with above requirement
2. Download composer
3. Clone this project 
4. Open your cmd and set path to this project
5. Run "composer install"
6. Run "php artisan key:generate"
7. Run "php artisan serve" 
8. Open your browser and go localhost:8000
9. Enjoy

Route
1. localhost:8000/kategori -> Kategori
2. localhost:8000/product -> Product
