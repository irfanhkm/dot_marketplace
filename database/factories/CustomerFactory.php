<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Customers;
use Faker\Generator as Faker;

    $factory->define(Customers::class, function (Faker $faker) {
        return [
            'email' => $faker->email,
            'first_name'=>$faker->name,
            'last_name'=>$faker->name,
            'address'=>$faker->address,
            'phone_number'=>$faker->randomDigit,
            'password'=> Hash::make('123')
        ];
});
