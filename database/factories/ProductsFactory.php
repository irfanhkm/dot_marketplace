<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Category;
use App\Products;
use Faker\Generator as Faker;

$factory->define(Products::class, function (Faker $faker) {

    return [
        //
        'nama' => Str::random(10),
        'unit_price' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 2)
    ];
});
