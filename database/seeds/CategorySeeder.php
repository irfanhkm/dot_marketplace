<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //old style seeder dengan data banyak
//        for ($i = 0; $i<2; $i++){
//            Category::create([
//                'name' => Str::random(10),
//            ]);
//        }
        // new style with factory
        factory(Category::class,10)->create();
    }
}
