<?php

use Illuminate\Database\Seeder;

class LoginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\User::create([
                'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password'=>Hash::make('password')
        ]);
    }
}
