<?php

use Illuminate\Database\Seeder;
use App\Products;
use App\Category;
use Faker\Generator as Faker;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        //get random category

        $category = Category::latest()->first();

        //insert prodduct

//        Products::create([
//            'nama' => Str::random(10),
//            'kategori_id' => $category->id,
//            'unit_price' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 2)
//        ]);

        factory(Products::class,1)->create([
            'kategori_id'=> $category->id
        ]);
    }
}
