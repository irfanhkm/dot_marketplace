<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnTableCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('customers', function (Blueprint $table) {
            $table->string('email',100)->nullable()->change();
            $table->string('first_name',45)->nullable()->change();
            $table->string('last_name',45)->nullable()->change();
            $table->text('address')->nullable()->nullable()->change();
            $table->string('phone_number',15)->nullable()->change();
            $table->string('password',80)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
