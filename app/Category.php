<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    
    protected $table = 'categories';

    protected $fillable = ['id','name','product_count'];

    public function products()
    {
        /**
         * category_id => foreign key dari tabel product
         * id => local/foreign key
         */
        return $this->hasMany(Products::class, 'kategori_id', 'id')->withTrashed();
    }

    public function delete()
    {
        $this->products()->delete();
        return parent::delete();
    }
}
