<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customers extends Model
{
    use SoftDeletes;
    public $table = 'customers';

    protected $fillable = ['email','first_name','last_name','address','phone_number','password'];

    public function order()
    {
        return $this->hasMany(Orders::class,'customer_id','id');
    }
}
