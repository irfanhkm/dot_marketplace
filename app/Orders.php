<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
    use SoftDeletes;
    //
    protected $table = 'orders';

    protected $fillable = ['customer_id','total'];

    public function orderDetails()
    {
        return $this->hasMany(OrderDetails::class, 'order_id', 'id');
    }

    public function customers()
    {
        return $this->hasOne(Customers::class, 'id', 'customer_id')->withTrashed();
    }

    public function delete()
    {
        $this->orderDetails()->delete();
        return parent::delete();
    }

    public function restore()
    {
        $this->orderDetails()->restore();
        return parent::restore();
    }
}
