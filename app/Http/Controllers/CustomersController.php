<?php

namespace App\Http\Controllers;

use App\Customers;
use App\Http\Requests\CustomersValidation;
use Exception;
use Illuminate\Http\Request;
use Session;

class CustomersController extends Controller
{
    //
    public function index()
    {
        $dataCustomer = Customers::query(); // Create empty query on category table

        // Search data
        // if search query exist and length more than one
        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $dataCustomer->where(
                "first_name", "like", "%" . request()->query("search")
            )->orWhere("last_name", "like", request()->query("search")."%");
        }

        // Query pagination
        $pagination = 5;
        $dataCustomer = $dataCustomer->orderBy('created_at', 'desc')->paginate($pagination);

        // Handle perpindahan page
        $number = 0; // Default page

        if (request()->has('page') && request()->get('page') > 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }

        return view('page.customer.index', compact('dataCustomer', 'number'));
    }

    public function store(Request $request, CustomersValidation $valid)
    {
        try{
            Customers::create([
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'phone_number'=>$request->phone_number,
                'email'=>$request->email,
                'password' => $request->password,
                'address'=>$request->address
            ]);
            Session::flash('success', 'Berhasil menambahkan data');
        }catch (Exception $e){
            report($e);
            Session::flash('fail', 'Gagal menambahkan data');
        }

        return redirect('customer');
    }

    public function create()
    {
        return view(
            'page.customer.form_add'
        );
    }

    public function show($id)
    {
        $dataCustomer = Customers::find($id);
        return view(
            'page.customer.detail',
            compact('dataCustomer')
        );
    }

    public function update(Request $request,$id,CustomersValidation $valid)
    {
        try{
            $data = Customers::find($id);
            $data->first_name =$request->first_name;
            $data->last_name =$request->last_name;
            $data->phone_number =$request->phone_number;
            $data->email =$request->email;
            if ($request->password)
            {
                $data->password =bcrypt($request->password);
            }
            $data->address =$request->address;
            $data->save();

            Session::flash('success','Berhasil mengubah data');

        }catch (Exception $e) {
            Session::flash('fail', 'Gagal mengubah data');
            report($e);
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        try{
            Customers::where('id',$id)->delete();
            Session::flash('success','Data berhasil dihapus');
        }catch (Exception $e){
            report($e);
            Session::flash('fail','Data gagal dihapus');
        }

        return redirect()->back();
    }

    public function edit($id)
    {
        $dataCustomer = Customers::find($id);
        return view(
            'page.customer.form_edit',
            compact('dataCustomer')
        );
    }

}
