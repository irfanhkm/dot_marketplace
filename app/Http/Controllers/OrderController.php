<?php

namespace App\Http\Controllers;

use App\Customers;
use App\Http\Requests\OrderValidation;
use App\OrderDetails;
use App\Orders;
use App\Products;
use Exception;
use Illuminate\Http\Request;
use Session;

class OrderController extends Controller
{
    public function index()
    {
        $dataOrder = Orders::query()->withTrashed(); // Create empty query on product table

        // Search data
        // if search query exist and length more than one
        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            // Searching berdasarkan nama customer
            $dataOrder->whereHas("customers", function($q){
                $q->where("first_name","like", "%" . request()->query("search"))->orWhere("last_name","like",request()->query("search") . "%");
            });
        }

        // Query pagination
        $pagination = 5;
        $dataOrder = $dataOrder->orderBy('created_at', 'desc')->paginate($pagination);

        // Handle perpindahan page
        $number = 0; // Default page

        if (request()->has('page') && request()->get('page') > 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }

        $dataCustomer = Customers::orderBy("first_name")->get();

        return view('page.order.index', compact('dataOrder', 'number', 'dataOrder', 'dataCustomer'));
    }

    public function store(Request $request, OrderValidation $valid)
    {
        try{
            if ($request->guest)
            {
                $customer = Customers::create([
                    'first_name' => $request->customer_id
                ]);
                $customer_id = $customer->id;
            } else {
                $customer_id = $request->customer_id;
            }

            Orders::create([
                'customer_id' => $customer_id,
                'total' => 0
            ]);

            Session::flash('success','Data berhasil ditambahkan');
        }catch (Exception $e){
            report($e);
            Session::flash('fail', 'Data gagal ditambahkan');
        }
        return redirect('order');
    }

    public function update()
    {

    }

    public function create()
    {
        $dataCustomer = Customers::orderBy("first_name")->get();
        return view(
            'page.order.form_add',compact('dataProduct','dataCustomer')
        );
    }

    public function show($id)
    {
        $dataProduct = Products::withTrashed()->get();
        $dataOrderDetails = OrderDetails::where('order_id', $id)->withTrashed()->get();
        $dataOrder = Orders::withTrashed()->find($id);
        $total = $dataOrder->total;
        return view("page.order.detail", compact('dataOrder', 'dataProduct', 'dataOrderDetails', 'total'));
    }

    public function addProduct(Request $request, $id)
    {
        $request->validate([
            'product_id' => 'required',
            'qty' => 'required|numeric',
        ]);

        try {
            \DB::beginTransaction();
            $dataOrderDetails = OrderDetails::withTrashed()->where('order_id', $id)->where('product_id', $request->product_id)->first();
            $dataProduct = Products::withTrashed()->find($request->product_id);
            $dataOrder = Orders::withTrashed()->find($id);
            if ($dataOrderDetails) {
                $dataOrderDetails->update([
                    'quantity' => $dataOrderDetails->quantity + $request->qty,
                    'price' => $dataOrderDetails->price + ($request->qty * $dataProduct->unit_price)
                ]);
                $total = $dataOrder->total + (($request->qty) * $dataOrderDetails->products->unit_price);
            } else {
                OrderDetails::create([
                    'order_id' => $id,
                    'product_id' => $request->product_id,
                    'quantity' => $request->qty,
                    'price' => $dataProduct->unit_price
                ]);
                $total = $dataOrder->total + ($request->qty * $dataProduct->unit_price);
            }
            $dataOrder->update([
                'total' => $total
            ]);
            \DB::commit();
            Session::flash('success', 'Data berhasil ditambahkan');
        } catch (Exception $e) {
            \DB::rollBack();
            report($e);
            Session::flash('fail', 'Data gagal ditambahkan');
        }
        return redirect()->back();

    }

    public function destroy($id)
    {
        try {
            \DB::beginTransaction();
            Orders::find($id)->delete();
            \DB::commit();
            Session::flash('success', 'Data berhasil dihapus');
        } catch (Exception $e) {
            \DB::rollBack();
            report($e);
            Session::flash('fail', 'Data gagal dihapus');
        }
        return redirect()->back();
    }

    public function restore($id)
    {
        try {
            Orders::withTrashed()->find($id)->restore();
            Session::flash('success', 'Data berhasil direstore');
        } catch (Exception $e) {
            eport($e);
            Session::flash('fail', 'Data gagal direstore');
        }
        return redirect()->back();
    }
}
