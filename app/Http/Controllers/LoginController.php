<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginValidation;
use Session;

class LoginController extends Controller
{
    public function doLogin(Request $request, LoginValidation $valid)
    {
        $credential = $request->except('_token');
        if (Auth::attempt($credential))
        {
            return redirect('product');
        }else{
            Session::flash('error','Email / Password Salah');
            return redirect()->back();
        }
    }
}
