<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    function index(){
        return 'user@index';
    }

    function hello($nama){
        return 'hello '.$nama;
    }
}
