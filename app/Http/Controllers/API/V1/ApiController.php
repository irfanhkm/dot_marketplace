<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function method_get()
    {
        $data = [
            'nama' => 'Irfan Hakim',
            'umur' => 18,
            'asal' => 'Malang',
            'sekolah' => [
                'SD Islam Al Azhar 34 Makassar',
                'SMP Plus Al Kausar',
                'SMK Telkom Malang',
                'Universitas Indonesia S1 Sistem Informasi',
                'Institut Teknologi Sepuluh Nopember S2 Manajemen',
            ]
        ];
        return apiResponeBuilder(200, $data);
    }


    public function method_post(Request $request)
    {
        $data = $request->all();
        if ($request->umur > 17) {
            $data['message'] = 'Boleh Mengurus Sim';
        } else {
            $data['message'] = 'Belum Cukup Umur';
        }
        return apiResponeBuilder(200, $data);
    }

    public function method_try(Request $request)
    {
        try {
            $data = $request->all();
            if (!$data) throw new \Exception('Data harus diisi');
            $code = 200;
            $respone = $data;
        } catch (\Exception $e) {
            $code = 500;
            $respone = $e->getMessage();
        }
        return apiResponeBuilder($code,$respone);
    }
}