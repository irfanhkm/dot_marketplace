<?php

namespace App\Http\Controllers\Api\V1;

use App\Customers;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomersValidation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Validator;

class CustomersController extends controller{

    public function index(){
        try{
            $dataCustomer = Customers::query(); // Create empty query on category table

            // Search data
            // if search query exist and length more than one
            if (request()->has("search") && strlen(request()->query("search")) >= 1) {
                $dataCustomer->where(
                    "first_name", "like", "%" . request()->query("search")
                )->orWhere("last_name", "like", request()->query("search")."%");
            }

            // Query pagination
            $pagination = 5;
            $dataCustomer = $dataCustomer->orderBy('created_at', 'desc')->paginate($pagination);

            // Handle perpindahan page
            $number = 0; // Default page

            if (request()->has('page') && request()->get('page') > 1) {
                $number += (request()->get('page') - 1) * $pagination;
            }
            $code = 200;
            $respone = array('number_start'=>$number+1,$dataCustomer);
        } catch (Exception $e) {
            $code = 500;
            $respone = $e->getMessage();
        }
        return apiResponeBuilder($code,$respone);
    }

    public function detail($id)
    {
        try{
            $data =  Customers::find($id);
            if (!$data) throw new Exception('Data tidak ada');
            $code = 200;
            $respone = $data;
        } catch (Exception $e) {
            $code = 500;
            $respone = $e->getMessage();
        }
        return apiResponeBuilder($code,$respone);
    }

    public function create(Request $request, CustomersValidation $valid)
    {
        try{
            $dataCustomer = Customers::create([
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'phone_number'=>$request->phone_number,
                'email'=>$request->email,
                'password'=> Hash::make($request->password),
                'address'=>$request->address
            ]);
            $code = 200;
            $respone = $dataCustomer;
        } catch (Exception $e) {
            $code = 500;
            $respone = $e->getMessage();
            if ($e instanceof ValidationException) $respone = $e->errors();
        }
        return apiResponeBuilder($code,$respone);
    }

}