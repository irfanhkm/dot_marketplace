<?php

namespace App\Http\Controllers\Api\V1;

use App\Category;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Validator;

class CategoryController extends controller{

    public function index()
    {
        try{
            $dataCategory = Category::query()->withTrashed(); // Create empty query on category table
            // Search data
            // if search query exist and length more than one
            if (request()->has("search") && strlen(request()->query("search")) >= 1) {
                $dataCategory->where(
                    "name", "like", "%" . request()->query("search") . "%"
                );
            }
            // Query pagination
            $pagination = 5;
            $dataCategory = $dataCategory->orderBy('created_at', 'desc')->paginate($pagination);
            if (!$dataCategory) throw new Exception('Data kosong');

            // Handle perpindahan page
            $number = 0; // Default page

            if (request()->has('page') && request()->get('page') > 1) {
                $number += (request()->get('page') - 1) * $pagination;
            }
            $code =  200;
            $respone = array('number_start'=>$number+1,$dataCategory);
        } catch (Exception $e) {
            $code = 500;
            $respone = $e->getMessage();
        }
        return apiResponeBuilder($code,$respone);
    }

    public function detail($id)
    {
        try{
            $data =  Category::find($id);
            if (!$data) throw new Exception('Data tidak ada');
            $code = 200;
            $respone = $data;
        } catch (Exception $e) {
            $code = 500;
            $respone = $e->getMessage();
        }
        return apiResponeBuilder($code,$respone);
    }

    public function create(Request $request)
    {
        try{
            $request->validate([
                'name' => 'required'
            ]);
            $dataCategory = Category::create([
                'name'=>$request->name
            ]);
            $code = 200;
            $respone = $dataCategory;
        } catch (Exception $e) {
            $code = 500;
            $respone = $e->getMessage();
            if ($e instanceof ValidationException) $respone = $e->errors();
        }
        return apiResponeBuilder($code,$respone);
    }
}
