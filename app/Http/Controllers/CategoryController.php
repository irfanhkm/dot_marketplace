<?php

namespace App\Http\Controllers;

use App\Category;
use App\Products;
use Illuminate\Http\Request;
use Session;
use Exception;
use App\Http\Requests\CategoryValidation;

class CategoryController extends Controller
{
    //
    public function index()
    {
        $dataCategory = Category::query()->withTrashed(); // Create empty query on category table

        // Search data
        // if search query exist and length more than one
        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $dataCategory->where(
                "name", "like", "%" . request()->query("search") . "%"
            );
        }

        // Query pagination
        $pagination = 5;
        $dataCategory = $dataCategory->orderBy('created_at', 'desc')->withCount('products')->paginate($pagination);

        // Handle perpindahan page
        $number = 0; // Default page

        if (request()->has('page') && request()->get('page') > 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }
        return view('page.category.index', compact('dataCategory', 'number'));
    }

    public function show($id)
    {
        // Find category by id dengan relasi products
        $product = Products::where('kategori_id',124)->get();
        $jumlah_product = count($product)+1;
        $category = Category::with("products")->withTrashed()->find($id);
        return view("page.category.detail", compact('category','jumlah_product'));
    }

    public function edit($id)
    {
        $dataCategory = Category::withTrashed()->find($id);
        return view(
            'page.category.edit',
            compact('dataCategory')
        );
    }

    public function store(Request $request, CategoryValidation $valid)
    {
        try{
            Category::create($request->except('_token'));
            Session::flash('success', 'Berhasil menambahkan data');
        }catch (Exception $e){
            report($e);
            Session::flash('fail', 'Gagal menambahkan data');
        }

        return redirect()->back();
    }

    public function update(Request $request,$id,CategoryValidation $valid)
    {
        try{
            Category::withTrashed()->where('id',$id)
                ->update($request->only('name'));

            Session::flash('success','Berhasil mengubah data');
        }catch(Exception $e){
            report($e);
            Session::flash('fail', 'Gagal mengubah data');
        }
        return redirect()->back();
    }

    public function destroy(Request $request)
    {
        try{
            Category::find($request->id)->delete();
            Session::flash('success','Data berhasil dihapus');
        }catch (Exception $e){
            report($e);
            Session::flash('fail', 'Gagal menghapus data');
        }

        return redirect('category');
    }

    public function restoreKategori($id)
    {
        try{
            Category::withTrashed()->where('id',$id)->restore();
            Session::flash('success','Data berhasil direstore');
        }catch(Exception $e){
            report($e);
            Session::flash('success','Data gagal direstore');
        }
        return redirect('category');
    }

    public function restoreProduct($id)
    {
        try{
            Products::withTrashed()->where('id',$id)->restore();
            Session::flash('success','Data berhasil direstore');
        }catch(Exception $e){
            report($e);
            Session::flash('success','Data gagal direstore');
        }
        return redirect()->back();
    }


}
