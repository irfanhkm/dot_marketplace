<?php

namespace App\Http\Controllers;

use App\Category;
use App\Products;
use Illuminate\Http\Request;
use Validator;
use Session;
use Exception;
use File;
use App\Http\Requests\ProductsValidation;

class ProductsController extends Controller
{
    //
    public function index()
    {
        $dataProduct = Products::query(); // Create empty query on product table

        // Search data
        // if search query exist and length more than one
        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $dataProduct->where(
                "nama", "like", "%" . request()->query("search") . "%"
            );
        }

        // Query pagination
        $pagination = 5;
        $dataProduct = $dataProduct->orderBy('created_at', 'desc')->paginate($pagination);

        // Handle perpindahan page
        $number = 0; // Default page

        if (request()->has('page') && request()->get('page') > 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }

        $dataCategory = Category::orderBy("name")->get();

        return view('page.product.index', compact('dataProduct', 'number','dataCategory'));
    }


    public function detail($id)
    {
        $dataProduct = Products::find($id);
        return view(
            'page.product.detail',
            compact('dataProduct')
        );
    }

    public function edit($id)
    {
        $dataProduct = Products::find($id);
        $dataCategory = Category::orderBy("name")->get();
        return view(
            'page.product.edit',
            compact('dataProduct','dataCategory')
        );
    }

    public function store(Request $request,ProductsValidation $valid)
    {
        try{
            //Memulai Data
            \DB::beginTransaction();

            $imageName = time().'.'.request()->photo->getClientOriginalExtension();
            request()->photo->move(public_path('images'), $imageName);
             //query store product
            $product = new Products();
            $product->nama =$request->nama;
            $product->kategori_id =$request->kategori_id;
            $product->unit_price =$request->unit_price;
            $product->photo = $imageName;
            $product->save();

             //update product count
            Category::find($request->kategori_id)->increment('product_count');
            \DB::commit();

            Session::flash('success', 'Berhasil menambahkan data');
            return redirect()->back();
        }catch (Exception $e){
            \DB::rollBack();
            Session::flash('fail', 'Data gagal ditambahkan');
            return redirect()->back()->with($e);
        }
    }

    public function update(Request $request,$id, ProductsValidation $valid)
    {
        try{
            $product = Products::find($id);;
            $product->nama =$request->nama;
            $product->kategori_id =$request->kategori_id;
            $product->unit_price =$request->unit_price;
            if ($request->photo){
                $imageName = time().'.'.request()->photo->getClientOriginalExtension();
                request()->photo->move(public_path('images'), $imageName);
//                File::delete('images/'.$product->photo);
                $product->photo = $imageName;
            }
            $product->save();
            Session::flash('success','Berhasil mengubah data');
        }catch (Exception $e){
            report($e);
            Session::flash('fail','Gagal mengubah data');
        }

        return redirect()->back();
    }

    public function delete(Request $request)
    {
        try{
            Products::find($request->id)->delete();
            Session::flash('success','Data berhasil dihapus');
        }catch (Exception $e){
            report($e);
            Session::flash('fail','Data gagal dihapus');
        }

        return redirect()->back();
    }


}
