<?php

namespace App\Http\Requests;

use http\Env\Request;
use Illuminate\Foundation\Http\FormRequest;

class OrderDetailsValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [];

        if ($request->products) {
            foreach ($request->products as $key => $value) {
                $rules["products.$key"] = 'required';
            }
        }
        if ($request->quantity) {
            foreach ($request->quantity as $key => $value) {
                $rules["quantity.$key"] = 'required|numeric';
            }
        }

        $rules['customer_id'] = 'required';

        return $rules;
    }
}
