<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CustomersValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'first_name'=>'required|min:2',
            'last_name'=>'required|min:2',
            'address'=>'required',
            'phone_number'=>'required|numeric',
            'email'=>'required|min:2|email|unique:customers,email,'.$request->get('id').',id',
            'password'=>$request->get('id') ? '': 'required'
        ];
    }
}
