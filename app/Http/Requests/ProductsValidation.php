<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ProductsValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $photos = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:20480';
        if ($request->photos){
            $photos = '';
        }
        return [
            'nama' => 'required',
            'kategori_id' => 'required',
            'unit_price' => 'required|max:10',
            'photo' => $photos,
        ];
    }
}
