<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use SoftDeletes;
    //
    protected $table = 'product';

    protected $fillable = ['nama','kategori_id','unit_price'];

    public function category() {
        return $this->hasOne(Category::class, "id", "kategori_id")->withTrashed();
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetails::class, 'product_id', 'id');
    }

}
