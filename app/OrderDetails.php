<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetails extends Model
{
    use SoftDeletes;

    protected $table = 'order_details';

    protected $fillable = ['order_id', 'product_id', 'quantity', 'price'];

    public function products()
    {
        return $this->hasOne(Products::class, 'id', 'product_id')->withTrashed();
    }

    public function order()
    {
        return $this->hasMany(Orders::class, 'id', 'order_id');
    }
}
