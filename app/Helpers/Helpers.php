<?php
if (! function_exists('apiResponeBuilder')) {
    function apiResponeBuilder($code, $data)
    {
        if ($code == 200) {
            $respone['status'] = 200;
            $respone['data'] = $data;
        } else {
            $respone['status'] = 500;
            $respone['message'] = $data;
        }
        return response()->json($respone, $code);
    }
}