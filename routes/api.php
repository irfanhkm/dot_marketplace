<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::prefix('api')->group(function(){});

/*
 * Routing API prefix = localhost/api/v1/
 * */
Route::prefix('v1')->group(function () {
    // route belajar
    Route::prefix('belajar')->group(function () {
        Route::get('cobaGet', 'API\V1\ApiController@method_get');
        Route::post('cobaPost', 'API\V1\ApiController@method_post');
        Route::post('cobaTry', 'API\V1\ApiController@method_try');
    });
    // route kategori
    Route::prefix('category')->group(function () {
        Route::get('/', 'API\V1\CategoryController@index');
        Route::get('/{id}', 'API\V1\CategoryController@detail');
        Route::post('/add', 'API\V1\CategoryController@create');
    });
    // route customer
    Route::prefix('customers')->group(function(){
        Route::get('/', 'API\V1\CustomersController@index');
        Route::get('/{id}', 'API\V1\CustomersController@detail');
        Route::post('/add', 'API\V1\CustomersController@create');
    });
});
