<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// static route
// return view welcome
Route::get('/', function () {
    return view('welcome');
});

/*
 * Route Login, logout
 * */
Route::get("/login", function(){
    if (Auth::user()){
        return redirect('product');
    }
    else{
        return view('auth.login'); // show login form
    }
})->name('login');

Route::post("/login", "LoginController@doLogin")->name("login.doLogin"); // controller login
Route::get('logout',function (){
   Auth::logout(); // Logout
   return redirect('login');
})->name('logout');

Route::middleware('auth')->group(function (){

    /*
    * Route Category
    * */
    // Route Category
    Route::resource('category', 'CategoryController');
    Route::prefix('category')->group(function () {
        Route::get('/{id}/restoreKategori','CategoryController@restoreKategori');
        Route::get('/{id}/restoreProduct','CategoryController@restoreProduct');
    });

    /*
     * Route Product
     * */
    Route::prefix('product')->group(function() {
        Route::get('/','ProductsController@index');
        Route::post('/store','ProductsController@store');
        Route::get('/{id}','ProductsController@detail');
        Route::get('/{id}/edit','ProductsController@edit');
        Route::post('/{id}/update','ProductsController@update');
        Route::get('/delete/{id}','ProductsController@delete');
    });

    /*
    * Route Customer
    * */
    Route::resource('customer', 'CustomersController');

    /*
    * Route Order
    * */
    Route::resource('order', 'OrderController',[
        'except' => ['update', 'destroy','edit']
    ]);

    Route::prefix('order')->group(function() {
        Route::post('/addProduct/{id}', 'OrderController@addProduct');
        Route::get('/delete/{id}', 'OrderController@destroy');
        Route::get('/restore/{id}', 'OrderController@restore');
    });
});
